# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
require 'yaml'

def ensure_agency(title)
  info "ensuring agency #{title}"
  Agency.find_by(title: title) || begin
    info "creating agency: #{title}"
    Agency.create!(title: title)
  end
end

def ensure_apartment(agency, attributes)
  info "ensuring apartment #{attributes} for agency #{agency.title}"
  agency.apartments.find_by(attributes) || begin
    info "creating apartment: #{attributes}"
    agency.apartments.create!(attributes)
  end
end

def info(*args)
  puts "\033[34;1m=> #{args.join(' ')}\033[0m"
end

ASSETS_PATH = File.join(Rails.root, 'lib', 'assets')
AGENCIES_PATH = File.join(ASSETS_PATH, 'rental_agencies.yml')
APARTMENTS_PATH = File.join(ASSETS_PATH, 'apartments.yml')

ActiveRecord::Base.transaction do
  agencies = YAML.load_file(AGENCIES_PATH)
  agencies.each do |title, priority|
    agency = ensure_agency(title)
    agency.update(priority: priority)
  end

  apartments = YAML.load_file(APARTMENTS_PATH)
  apartments.each do |_apartment|
    agency = Agency.find_by(title: _apartment['rental_agency'])
    apartment = ensure_apartment(agency,
      address: _apartment['address'],
      apartment: _apartment['apartment'],
      city: _apartment['city']
    )
    apartment.update(price: _apartment['price'])
  end
end