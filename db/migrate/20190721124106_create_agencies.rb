class CreateAgencies < ActiveRecord::Migration[5.2]
  def change
    create_table :agencies do |t|
      t.string :title, null: false, unique: true
      t.integer :priority

      t.timestamps
    end
  end
end
