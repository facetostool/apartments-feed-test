class CreateApartments < ActiveRecord::Migration[5.2]
  def change
    create_table :apartments do |t|
      t.references :agency, foreign_key: true, index: true
      t.string :address
      t.string :price
      t.string :city
      t.string :apartment

      t.timestamps
    end
  end
end
