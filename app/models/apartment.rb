# == Schema Information
#
# Table name: apartments
#
#  id         :bigint           not null, primary key
#  address    :string
#  price      :string
#  agency_id  :bigint
#  city       :string
#  apartment  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Apartment < ApplicationRecord
  belongs_to :agency

  scope :filtered, -> do
    joins(:agency)
      .select('DISTINCT ON ("apartments"."address","apartments"."city","apartments"."apartment") "apartments".* ')
      .order(:address, :city, :apartment, priority: :desc)
  end

end
