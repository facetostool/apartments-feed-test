# == Schema Information
#
# Table name: agencies
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AgencySerializer
  include FastJsonapi::ObjectSerializer

  attributes :title, :priority

  has_many :apartments
end
