# == Schema Information
#
# Table name: apartments
#
#  id         :bigint           not null, primary key
#  address    :string
#  price      :string
#  agency_id  :bigint
#  city       :string
#  apartment  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ApartmentSerializer
  include FastJsonapi::ObjectSerializer

  attributes :address, :price, :apartment, :city

  belongs_to :agency
end
