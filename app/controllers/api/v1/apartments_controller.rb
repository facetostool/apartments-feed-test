class Api::V1::ApartmentsController < Api::V1::BaseController
  def index
    render json: ApartmentSerializer.new(
      Apartment.includes(:agency).filtered, { include: [:agency] },
    )
  end
end