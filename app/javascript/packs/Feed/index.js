import React from 'react'
import ReactDOM from 'react-dom'
import Apartments from '../Apartments'

ReactDOM.render((
    <Apartments />
), document.getElementById('feed'))
