import React from 'react'
import Attribute from './attribute'

function Item(props) {
    const attributes = props.data.attributes
    const agency = props.included.find( agency =>
        agency.type == 'agency' && agency.id == props.data.relationships.agency.data.id
    )
    return (
        <div className='apartment-item'>
            <Attribute name={'agency'} value={agency.attributes.title}/>
            <Attribute name={'priority'} value={agency.attributes.priority}/>
            <Attribute name={'address'} value={attributes.address}/>
            <Attribute name={'city'} value={attributes.city}/>
            <Attribute name={'apartment'} value={attributes.apartment}/>
        </div>
    )
}

export default Item