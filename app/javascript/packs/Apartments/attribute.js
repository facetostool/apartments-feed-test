import React from 'react'

function Attribute(props) {
    return (
        <div className='attribute'>
            <span className='name'>{props.name}:</span>
            <span className='value'>{props.value}</span>
        </div>
    )
}

export default Attribute