import React, {Component} from 'react'
import Item from './item'

class Apartments extends Component {
    constructor() {
        super()
        this.state = {
            loading: true,
            apartments: {}
        }
    }

    componentDidMount() {
        fetch("/api/v1/apartments")
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loading: false,
                    apartments: data
                })
            })
    }

    render() {
        var content
        if (this.state.loading) {
            content = "loading"
        } else {
            content = this.state.apartments.data.map( apartment =>
                <Item data={ apartment } key={apartment.id} included={ this.state.apartments.included } />
            )
        }
        return (
            <div>
                {content}
            </div>
        )
    }
}

export default Apartments