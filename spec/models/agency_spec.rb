# == Schema Information
#
# Table name: agencies
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Agency, type: :model do
  describe 'associations' do
    it { should have_many(:apartments).class_name('Apartment') }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
  end
end
