# == Schema Information
#
# Table name: apartments
#
#  id         :bigint           not null, primary key
#  address    :string
#  price      :string
#  agency_id  :bigint
#  city       :string
#  apartment  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Apartment, type: :model do
  describe 'associations' do
    it { should belong_to(:agency).class_name('Agency') }
  end

  describe '#filtered' do
    let(:agency_high_priority) { create(:agency, priority: 10) }
    let(:agency_low_priority) { create(:agency, priority: 1) }

    context 'when exist identical apartments' do
      let(:apartment1) { create(:apartment, agency: agency_high_priority) }
      let(:apartment2) {
        create(:apartment,
          agency: agency_low_priority,
          address: apartment1.address,
          apartment: apartment1.apartment,
          city: apartment1.city)
      }
      let(:apartment3) { create(:apartment, agency: agency_low_priority) }

      it 'shows apartment by agency with higher priority' do
        expect(described_class.filtered).to match_array [apartment1, apartment3]
      end
    end
  end
end
