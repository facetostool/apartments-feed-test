# == Schema Information
#
# Table name: agencies
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  priority   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :agency do
    title { Faker::Hipster.word }
    priority { Faker::Number.within(1..10) }
  end
end
