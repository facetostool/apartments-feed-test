# == Schema Information
#
# Table name: apartments
#
#  id         :bigint           not null, primary key
#  address    :string
#  price      :string
#  agency_id  :bigint
#  city       :string
#  apartment  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :apartment do
    price { Faker::Number.within(100..1000) }
    agency
    city {  Faker::Address.city }
    address { Faker::Address.street_address }
    apartment { Faker::Address.secondary_address }
  end
end
