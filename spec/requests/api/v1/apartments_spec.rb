require 'spec_helper'

describe 'api/v1/apartments', type: :request do
  let!(:apartment) { create(:apartment) }
  let(:agency) { apartment.agency }

  describe 'GET /' do
    before {  get api_v1_apartments_path(format: :json) }
    it 'returns code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns single record' do
      data = JSON.parse(response.body).deep_symbolize_keys
      expect(data[:data].size).to eq(1)
      expect(data[:included].size).to eq(1)
    end

    it 'returns right data' do
      data = JSON.parse(response.body).deep_symbolize_keys

      apartment_data = data[:data][0]
      expect(apartment_data).to include(
        id: apartment.id.to_s,
        type: 'apartment',
        attributes: {
          address: apartment.address,
          price: apartment.price,
          apartment: apartment.apartment,
          city: apartment.city,
        },
        relationships: {
          agency: {
            data: {
              id: agency.id.to_s,
              type: 'agency',
            }
          }
        }
      )
    end

    it 'includes agency data into the response' do
      data = JSON.parse(response.body).deep_symbolize_keys
      included = data[:included][0]

      expect(included).to include(
        id: agency.id.to_s,
        type: 'agency',
        attributes: {
          title: agency.title,
          priority: agency.priority,
        },
        relationships: {
          apartments: {
            data: [
              {
                id: apartment.id.to_s,
                type: 'apartment',
              }
            ]
          }
        }
      )
    end
  end
end