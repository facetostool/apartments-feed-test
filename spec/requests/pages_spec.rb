require 'spec_helper'

describe 'Pages', type: :request do

  describe 'GET /' do
    it 'works' do
      get root_path
      expect(response).to have_http_status(200)
      expect(response).to render_template(:index)
    end
  end

  describe 'GET /feed' do
    it 'works' do
      get feed_path
      expect(response).to have_http_status(200)
      expect(response).to render_template(:feed)
    end
  end
end