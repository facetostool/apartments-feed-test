Rails.application.routes.draw do
  root 'pages#index'

  get 'feed', to: 'pages#feed'

  namespace :api do
    namespace :v1 do
      resources :apartments, only: [:index]
    end
  end
end
